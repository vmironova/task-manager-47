package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getPort();

    @NotNull
    String getHost();

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

}
