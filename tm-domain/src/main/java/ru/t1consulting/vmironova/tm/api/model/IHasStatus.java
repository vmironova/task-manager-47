package ru.t1consulting.vmironova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
